import System.Environment (getArgs)
import FileTools

data Mode = Inc | Dec deriving (Show,Read,Eq)

readMode :: String -> Maybe Mode
readMode "inc" = Just Inc
readMode "dec" = Just Dec
readMode _ = Nothing

brightnessDir = "/sys/class/backlight/intel_backlight/"
maxBrightnessFilePath = brightnessDir ++ "max_brightness"
brightnessFilePath = brightnessDir ++ "brightness"

data BrightnessInfo = BrightnessInfo {
  currentBrightness :: Int,
  maxBrightness :: Int
}


readBrightness :: IO BrightnessInfo
readBrightness = do
  maxBrightness <- readIntFromFileOr maxBrightnessFilePath 0 
  brightness <- readIntFromFileOr brightnessFilePath 0 
  return $ BrightnessInfo brightness maxBrightness

clip lowerLimit upperLimit = max lowerLimit . min upperLimit 

changeBrightness :: (Double -> Double) -> (Double -> Double) -> IO ()
changeBrightness f1 f2 = do

  brightnessInfo <- readBrightness
  let cur = currentBrightness brightnessInfo

  let double_cur = fromIntegral cur
  let cur' = round (f1 double_cur)
  let cur'' = if cur' /= cur then cur' else round (f2 double_cur)

  let mx = maxBrightness brightnessInfo
  let newBrightness = clip 0 mx cur''
  writeIntToFile brightnessFilePath newBrightness

adjFactor :: Double
adjFactor = 1.5


main = do
  args <- getArgs
  case args of
    [] -> do
            brtnsInfo <- readBrightness
            let cur = fromIntegral (currentBrightness brtnsInfo) :: Double
            let mx = fromIntegral (maxBrightness brtnsInfo) :: Double
            print $ cur / mx * 100
    [x] -> case readMode x of
            Nothing -> print "hä?"
            Just Inc -> changeBrightness (* adjFactor) (+ 1)
            Just Dec -> changeBrightness (/ adjFactor) (\x -> x - 1)
